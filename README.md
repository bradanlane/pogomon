# PogoMon

PCB and Electronics Test Probes
<img align="right" src="files/pogomon_small.png">

## What is it?

The PogoMon is a small PCB with a long fine point spring pin _(often called a pogo pin)_ at one end. It has a standard 2-pin 0.1" header connector with the top pin for the probe and the bottom pin is an optional GND connector.

The PogoMon has an additional pivot pin location and an arc of pin locations. These may be used to stack multiple PogoMon together in-line or with a varying degree of offset. _(The pivot is connected to the GND pin to allow multiple PogoMon to have a shared GND.)_

## How do I use it?

The PogoMon is designed to be held in place using an existing 'third hand' type setup. It works with the flexible arm as well as rigid like bent setups. Any number of other holding methods may be used including 3D printed grippers, weighted bases, magnetic bases, etc.

[Video Demonstration](https://vimeo.com/682264333)

### Tips for using a flexible arm with magnetic base and alligator clip

When using a flexible arm type 'third hand', the arm provides little resistance against the PogoMon pogo pin spring.

- move the flexible arm close to the location to be probed
- hold the PogoMon onto the target location and press down to compress the pogo pin
- finish moving the flexible arm into position so the alligator clip will grasp the top of the PogoMon
- clip the top of the PogoMon
- let go of the PogoMon; the pogo pin spring will push up against the resistance of the arm

It may be necessary to make adjustments. It may be helpful if the flexible arm has some weight at the clip end.

### Tips for stacking multiple PogoMon

The PogoMon may be used individually or stacked. The pivot pin location and arc pin locations allow for stacked PogoMon to have varying offset. When a very small offset is needed, the plastic spacer on the pivot pin may be removed. To avoid shorting stacked PogoMon, you may add a layer of insulating tape.
 
There is a small amount of give to the pogo pin. While it is possible to slightly bend the tips together, avoid too much force.

## License

The PogoMon design is open source under the MIT license
